/*----------------------------------------------------------------------------*/
/* A test program to test the FrFileIInMemory object                          */
/*----------------------------------------------------------------------------*/

#include <FrameL.h>

/*------------------------------------------------------------- main ---------*/
int main(int argc, char *argv[])
/*------------------------------------------------------------- main ---------*/
{
  FrameH* frame;
  int size;

  /*---------first create and fill the inMemory buffer with multiple frames---*/
  FrFile *iFile = FrFileINew("../data/test.gwf");

  FrFile *memFile =  FrFileIInMemoryNew();

  int bufSize = 100000;
  while((frame = FrameRead(iFile)) != NULL) {
    printf(" read frame %d\n",frame->GTimeS);
    char *buf = malloc(bufSize);
    size = FrameWriteToBuf(frame, 0, buf, bufSize, 0);
    printf(" %d bytes writen to buf\n", size);
    FrFileIInMemoryAddBuffer(memFile, buf,size, 8);
    free(buf);
    FrameFree(frame);}

  /*----------------------------then check boundaries and extract a vector---*/
  double tStart = FrFileITStart(memFile);
  double tEnd   = FrFileITEnd(memFile);

  printf(" tStart=%.0f tEnd=%.0f\n", tStart, tEnd);

  FrVect *vect = FrFileIGetVect(memFile, "Adc0", 925484678.3, 3.3);
  FrVectDump(vect, stdout, 2);
 
  /*----------------------------------compare the vector to a direct read---*/
  iFile = FrFileINew("../data/test.gwf");
  vect = FrFileIGetVect(iFile, "Adc0", 925484678.3, 3.3);
  FrVectDump(vect, stdout, 2);

  return(0);
}
