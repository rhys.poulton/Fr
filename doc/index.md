# Frame Library

## Introduction

A frame is a unit of information containing all the information necessary for the understanding of the interferometer behavior over a finite time interval which integrates several samplings. It contains thus not only the sampling performed during the integrated time interval, but also those performed at a frequency smaller than the frame frequency.

See the FrameL documentation for more details.

## Links

* Latest version: [http://software.igwn.org/sources/source/framel-8.39.2.tar.xz](http://software.igwn.org/sources/source/framel-8.39.2.tar.xz)

